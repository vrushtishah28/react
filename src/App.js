import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import "./App.css";
import Routes from "./routes";
import Layout from "./component/layout/layout";

function App() {
  return (
    <Layout>
      <Routes></Routes>
    </Layout>
  );
}
export default App;
