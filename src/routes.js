import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AgGridPage from "./containers/Ag-Grid/AgGridPage";
import home from "./containers/Home/home";
import AgGridThemes from "./containers/Ag-Grid/AgGridThemes";
import AgGridScroll from "./containers/Ag-Grid/AgGridScroll";
import AgGridAutoWidth from "./containers/Ag-Grid/AgGridAutoWidth";
import ReactToastify from "./containers/Toast/ReactToastify";
import CardLoader from "./containers/Loader/CardLoader";
import SkeletonPage from "./containers/Skeleton/SkeletonPage";

function Routes() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/Skeleton" component={SkeletonPage} />
          <Route path="/CardLoader" component={CardLoader} />
          <Route path="/Toast" component={ReactToastify} />
          <Route path="/AgGrid-scroll" component={AgGridScroll} />
          <Route path="/AgGrid-noscroll" component={AgGridAutoWidth} />
          <Route path="/AgGrid-themes" component={AgGridThemes} />
          <Route path="/AgGridPage" component={AgGridPage} />
          <Route path="/" component={home} />
        </Switch>
      </div>
    </Router>
  );
}
export default Routes;
