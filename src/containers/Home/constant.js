export const ComponentMainPage = [
  {
    title: "AG-GRID Components",
    description: "This shows various Ag-grid Components",
    imgUrl:
      "https://pbs.twimg.com/profile_images/961966400016404481/oEXeZFEy_400x400.jpg",
    link: "/AgGridPage",
  },
  {
    title: "Toastify Components",
    description: "This shows all kinds of toasts",
    imgUrl: "https://miro.medium.com/max/2984/1*FOvfJzycaPXdse2NZcxMDg.png",
    link: "/Toast",
  },
  {
    title: "Loader Components",
    description: "This one shows Loader Components",
    imgUrl: "https://i.gifer.com/8kpb.gif",
    link: "/CardLoader",
  },
  {
    title: "Skeleton Component",
    description: "This one shows Skeleton Effect",
    imgUrl: "https://miro.medium.com/max/2876/1*tAzqw_10J0uXEPeVzmlvLA.gif",
    link: "/Skeleton",
  },
];
