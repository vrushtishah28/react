import React from "react";
import CardList from "../../component/cards/CardList";
import { ComponentMainPage } from "./constant";

function home() {
  return (
    <section>
      <div class="container">
        <h1>Components</h1>
        <CardList className="cardlist" data={ComponentMainPage}></CardList>
      </div>
    </section>
  );
}

export default home;
