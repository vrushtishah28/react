import React, { useState } from "react";
import { Helmet } from "react-helmet";
import Skeleton from "react-loading-skeleton";
import "./blog.css";

function SkeletonComponent() {
  const [showSkeleton, setShowSkeleton] = useState(0);
  return (
    <div className="skeleton-wrapper">
      <Helmet>
        <meta charset="utf-8" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <meta name="description" content="" />
        <meta
          name="author"
          content="Mark Otto, Jacob Thornton, and Bootstrap contributors"
        />
        <meta name="generator" content="Jekyll v3.8.6" />
        <title>Blog Template · Bootstrap</title>

        <link
          rel="canonical"
          href="https://getbootstrap.com/docs/4.4/examples/blog/"
        />

        <link
          href="/docs/4.4/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossorigin="anonymous"
        />

        <link
          rel="apple-touch-icon"
          href="/docs/4.4/assets/img/favicons/apple-touch-icon.png"
          sizes="180x180"
        />
        <link
          rel="icon"
          href="/docs/4.4/assets/img/favicons/favicon-32x32.png"
          sizes="32x32"
          type="image/png"
        />
        <link
          rel="icon"
          href="/docs/4.4/assets/img/favicons/favicon-16x16.png"
          sizes="16x16"
          type="image/png"
        />
        <link
          rel="manifest"
          href="/docs/4.4/assets/img/favicons/manifest.json"
        />
        <link
          rel="mask-icon"
          href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg"
          color="#563d7c"
        />
        <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico" />
        <meta
          name="msapplication-config"
          content="/docs/4.4/assets/img/favicons/browserconfig.xml"
        />
        <meta name="theme-color" content="#563d7c" />

       
      </Helmet>

      <div class="container">
        <header class="blog-header py-3">
          <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="col-4 pt-1">
              <a class="text-muted" href="#">
                <Skeleton width={70} />
              </a>
            </div>
            <div class="col-4 text-center">
              <a class="blog-header-logo text-dark" href="#">
                <Skeleton width={150} />
              </a>
            </div>
            <div class="col-4 d-flex justify-content-end align-items-center">
              <div class="mr-2">
                <Skeleton width={30} />
              </div>
              <Skeleton width={100} />
            </div>
          </div>
        </header>

        <div class="nav-scroller py-1 mb-2">
          <nav class="nav d-flex justify-content-between">
            <Skeleton width={70} />

            <Skeleton width={70} />

            <Skeleton width={70} />

            <Skeleton width={70} />

            <Skeleton width={70} />

            <Skeleton width={70} />

            <Skeleton width={70} />

            <Skeleton width={70} />

            <Skeleton width={70} />

            <Skeleton width={70} />

            <Skeleton width={70} />

            <Skeleton width={70} />
          </nav>
        </div>

        <div class="jumbotron p-4 p-md-5 text-white rounded bg-light">
          <div class="col-md-6 px-0">
            <h1 class="display-4 font-italic">
              <Skeleton />
            </h1>
            <p class="lead my-3">
              <Skeleton count={3} />
            </p>
            <p class="lead mb-0">
              <a href="#" class="text-white font-weight-bold">
                <Skeleton width={200} />
              </a>
            </p>
          </div>
        </div>

        <div class="row mb-2">
          <div class="col-md-6">
            <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
              <div class="col p-4 d-flex flex-column position-static">
                <strong class="d-inline-block mb-2 text-primary">
                  <Skeleton width={70} />
                </strong>
                <h3 class="mb-0">
                  <Skeleton />
                </h3>
                <div class="mb-1 text-muted">
                  <Skeleton width={70} />
                </div>
                <p class="card-text mb-auto">
                  <Skeleton count={3} />
                </p>
                <a href="#" class="stretched-link">
                  <Skeleton width={100} />
                </a>
              </div>
              <div class="col-auto d-none d-lg-block ">
                <div style={{ position: "relative", top: "-5px" }}>
                  <Skeleton height={250} width={200} />
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
              <div class="col p-4 d-flex flex-column position-static">
                <strong class="d-inline-block mb-2 text-success">
                  <Skeleton width={70} />
                </strong>
                <h3 class="mb-0">
                  <Skeleton />
                </h3>
                <div class="mb-1 text-muted">
                  <Skeleton width={70} />
                </div>
                <p class="mb-auto">
                  <Skeleton count={3} />
                </p>
                <a href="#" class="stretched-link">
                  <Skeleton width={100} />
                </a>
              </div>
              <div class="col-auto d-none d-lg-block">
                <div style={{ position: "relative", top: "-5px" }}>
                  <Skeleton height={250} width={200} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <main role="main" class="container">
        <div class="row">
          <div class="col-md-8 blog-main">
            <h3 class="pb-4 mb-4 font-italic border-bottom">
              <Skeleton width={250} />
            </h3>

            <div class="blog-post">
              <h2 class="blog-post-title">
                <Skeleton width={350} />
              </h2>
              <p class="blog-post-meta">
                <Skeleton width={250} />
              </p>

              <p>
                <Skeleton count={2} />
              </p>
              <hr />
              <p>
                <Skeleton count={3} />
              </p>
              <blockquote>
                <p>
                  <Skeleton count={2} />
                </p>
              </blockquote>
              <p>
                <Skeleton count={2} />
              </p>
              <h2>
                <Skeleton width={150} />
              </h2>
              <p>
                <Skeleton count={3} />
              </p>
              <h3>
                {" "}
                <Skeleton width={200} />
              </h3>
              <p>
                <Skeleton />
              </p>
              <pre>
                <code>
                  {" "}
                  <Skeleton width={150} />
                </code>
              </pre>
              <p>
                <Skeleton count={2} />
              </p>
              <h3>
                {" "}
                <Skeleton width={150} />
              </h3>
              <p>
                <Skeleton count={4} />
              </p>
              <div className="d-flex mw-100">
                <div className="mx-1">
                  <Skeleton height={10} width={10} circle={true} />
                </div>

                <Skeleton width={200} />
              </div>
              <div className="d-flex mw-100">
                <div className="mx-1">
                  <Skeleton height={10} width={10} circle={true} />
                </div>

                <Skeleton width={200} />
              </div>
              <div className="d-flex mw-100">
                <div className="mx-1">
                  <Skeleton height={10} width={10} circle={true} />
                </div>

                <Skeleton width={200} />
              </div>
              <p>
                <Skeleton />
              </p>
              <div className="d-flex mw-100">
                <div className="mx-1">
                  <Skeleton height={10} width={10} circle={true} />
                </div>

                <Skeleton width={300} />
              </div>
              <div className="d-flex mw-100">
                <div className="mx-1">
                  <Skeleton height={10} width={10} circle={true} />
                </div>

                <Skeleton width={300} />
              </div>
              <div className="d-flex mw-100">
                <div className="mx-1">
                  <Skeleton height={10} width={10} circle={true} />
                </div>

                <Skeleton width={300} />
              </div>
              <p>
                <Skeleton />
              </p>
            </div>

            <div class="blog-post">
              <h2 class="blog-post-title">
                {" "}
                <Skeleton width={350} />
              </h2>
              <p class="blog-post-meta">
                <Skeleton width={250} />
              </p>

              <p>
                <Skeleton count={3} />
              </p>
              <blockquote>
                <p>
                  <Skeleton count={2} />
                </p>
              </blockquote>
              <p>
                <Skeleton count={2} />
              </p>
              <p>
                <Skeleton count={3} />
              </p>
            </div>

            <div class="blog-post">
              <h2 class="blog-post-title">
                {" "}
                <Skeleton width={250} />
              </h2>
              <p class="blog-post-meta">
                <Skeleton width={200} />
              </p>

              <p>
                <Skeleton count={4} />
              </p>
              <div className="d-flex mw-100">
                <div className="mx-1">
                  <Skeleton height={10} width={10} circle={true} />
                </div>

                <Skeleton width={300} />
              </div>
              <div className="d-flex mw-100">
                <div className="mx-1">
                  <Skeleton height={10} width={10} circle={true} />
                </div>

                <Skeleton width={300} />
              </div>
              <div className="d-flex mw-100">
                <div className="mx-1">
                  <Skeleton height={10} width={10} circle={true} />
                </div>

                <Skeleton width={300} />
              </div>
              <p>
                <Skeleton count={2} />
              </p>
              <p>
                <Skeleton />
              </p>
            </div>

            <nav class="blog-pagination">
              <div className="mr-2 d-inline-block">
                <Skeleton width={100} />
              </div>
              <Skeleton width={100} />
            </nav>
          </div>

          <aside class="col-md-4 blog-sidebar">
            <div class="p-4 mb-3 bg-light rounded">
              <h4 class="font-italic">
                {" "}
                <Skeleton width={100} />
              </h4>
              <p class="mb-0">
                <Skeleton count={4} />
              </p>
            </div>

            <div class="p-4">
              <h4 class="font-italic">
                {" "}
                <Skeleton width={130} />
              </h4>
              <div className="d-flex flex-column">
                <Skeleton width={100} />

                <Skeleton width={100} />
                <Skeleton width={100} />
                <Skeleton width={100} />
                <Skeleton width={100} />
                <Skeleton width={100} />
                <Skeleton width={100} />
                <Skeleton width={100} />
                <Skeleton width={100} />
                <Skeleton width={100} />
                <Skeleton width={100} />
                <Skeleton width={100} />
                <Skeleton width={100} />
              </div>
            </div>

            <div class="p-4">
              <h4 class="font-italic">
                {" "}
                <Skeleton width={130} />
              </h4>
              <div className="d-flex flex-column">
                <Skeleton width={100} />

                <Skeleton width={100} />

                <Skeleton width={100} />
              </div>
            </div>
          </aside>
        </div>
      </main>

      <div class="blog-footer">
        <p>
          <Skeleton width={300} />
        </p>
        <p>
          <Skeleton width={150} />
        </p>
      </div>
    </div>
  );
}

export default SkeletonComponent;
