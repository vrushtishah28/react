import React, { useState } from "react";
import { toast, cssTransition } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Slide, Flip, Bounce } from "react-toastify";
import "./ReactToastify.css";
import { toastDirectionData, toastType } from "../../component/Toast/constant";
function ReactToastify() {
  const [direction, setDirection] = useState(toastDirectionData[0].value);
  const [toastTypeFlag, setToastTypeFlag] = useState(toastType[0].type);
  const [toastTransitions, setToastTransitions] = useState(0);
  const transitionMap = {
    0: Slide,
    1: Flip,
    2: Bounce,
  };
  const color = {
    success: "toast-success",
    error: "toast-error",
    info: "toast-info",
    warning: "toast-warning",
    default: "toast-default",
  };
  const showToast = (type) => {
    const callingFunction = {
      success: toast.success,
      error: toast.error,
      info: toast.info,
      warning: toast.warning,
      default: toast,
    };

    callingFunction[type]("Wow so easy!", {
      position: direction,
      autoClose: false,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      className: `text-dark bg-light ${color[toastTypeFlag]}`,
      transition: transitionMap[toastTransitions],
    });
  };
  toast.configure();
  return (
    <>
      <div class="toastify-wrapper">
        <h1 className="text-center mb-5">React Toastify</h1>
        <section>
          <div className="container">
            <div className="row">
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-6">
                    <h2 className="mb-3">Direction</h2>
                    <div className="radio-group-direction">
                      {toastDirectionData.map((item) => {
                        const { name, value } = item;
                        return (
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              value={value}
                              type="radio"
                              name={name}
                              id={value}
                              checked={direction === value}
                              onChange={(e) => {
                                setDirection(e.target.value);
                              }}
                            />
                            <label className="form-check-label" for={value}>
                              {value}
                            </label>
                          </div>
                        );
                      })}
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div className="col-md-3">
                      <h2 className="mb-3">Type</h2>
                      <div className="radio-group-type">
                        {toastType.map((item) => {
                          const { type, name } = item;
                          return (
                            <div className="form-check">
                              <input
                                value={type}
                                className="form-check-input"
                                type="radio"
                                name={name}
                                id={type}
                                checked={toastTypeFlag === type}
                                onChange={(e) => {
                                  setToastTypeFlag(e.target.value);
                                  console.log(toastTypeFlag);
                                }}
                              />
                              <label className="form-check-label" for={type}>
                                {type}
                              </label>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row mt-5">
                  <div class="col-md-8">
                    <h2 className="mb-3">Transition Effects</h2>
                    <div className="d-flex justify-content-between">
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          value="0"
                          type="radio"
                          name="transition-toast"
                          id="Slide"
                          checked={toastTransitions === 0}
                          onChange={(e) => {
                            setToastTransitions(0);
                          }}
                        />
                        <label className="form-check-label" for="Slide">
                          Slide
                        </label>
                      </div>
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          value="1"
                          type="radio"
                          name="transition-toast"
                          id="Flip"
                          checked={toastTransitions === 1}
                          onChange={(e) => {
                            setToastTransitions(1);
                          }}
                        />
                        <label className="form-check-label" for="Flip">
                          Flip
                        </label>
                      </div>
                      <div className="form-check">
                        <input
                          className="form-check-input"
                          value="2"
                          type="radio"
                          name="transition-toast"
                          id="Bounce"
                          checked={toastTransitions === 2}
                          onChange={(e) => {
                            setToastTransitions(2);
                          }}
                        />
                        <label className="form-check-label" for="Bounce">
                          Bounce
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <h2 className="mb-3">Toast Container</h2>
                <div className="syntax-highlighter">
                  <p className="code-container">
                    {`const showToast = () => {
                toast.${toastTypeFlag}(" Wow so easy!", {
                position: ${direction},
                autoClose: false,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                 });
                }
                 <button className="btn-primary" 
                 onClick={() => showToast()}>
                         Show Toast
                </button>`}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className="container">
            <button
              className="btn-primary py-2 px-4"
              onClick={() => showToast(toastTypeFlag, direction)}
            >
              Show Toast
            </button>
          </div>
        </section>
      </div>
    </>
  );
}

export default ReactToastify;
