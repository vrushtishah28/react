import React, { useState } from "react";
import Grid from "../../component/Ag-Grid/Grid";
import { theme } from "../../component/Ag-Grid/constant";

function AgGridThemes() {
  const [selectedTheme, setSelectedTheme] = useState(theme[0]);
  return (
    <>
      <section>
        <div class="container">
          <h1>Ag-Grid Themes</h1>

          <select
            className="form-control"
            value={selectedTheme}
            onChange={(e) => setSelectedTheme(e.target.value)}
          >
            {theme.map((item) => (
              <option value={item}>{item}</option>
            ))}
          </select>
        </div>
      </section>
      <section>
        <div class="container mt-4">
          <div class="row">
            <div class="col-md-6 m-auto">
              <h2>Syntax</h2>
              <div className="syntax-highlighter">
                <p className="code-container">
                  {`import "ag-grid-community/dist/styles/${selectedTheme}.css";     
        <div className='${selectedTheme}'>
                <AgGridReact
                columnDefs={gridData.columnDefs}
                rowData={gridData.rowData}/>
        </div>`}
                </p>
              </div>
            </div>
            <div class="col-md-6">
              <Grid
                theme={selectedTheme}
                height="300px"
                width="100%"
                autowidth
              ></Grid>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default AgGridThemes;
