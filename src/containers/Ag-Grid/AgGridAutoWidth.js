import React, { useState } from "react";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-blue.css";
import Grid from "../../component/Ag-Grid/Grid";

function AgGridAutoWidth() {
  return (
    <>
      <div
        className="ag-theme-blue"
        style={{
          margin: "auto",
          maxWidth: "80%",
          height: 400,
        }}
      >
        <h1>Ag Grid Without ScrollBar[Auto Resizing]</h1>
        <Grid height="300px" width="100%" autowidth></Grid>
        <p>
          We are using Ag Grid Without ScrollBar.Where on minimizing the browser
          it will fit by itself within the browser.And on refreshing auto height
          width will be set as it was before.
        </p>
      </div>
    </>
  );
}
export default AgGridAutoWidth;
