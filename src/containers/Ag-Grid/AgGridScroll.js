import React, { useState } from "react";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-blue.css";
import { getAllPost } from "../../component/Ag-Grid/services";
import Grid from "../../component/Ag-Grid/Grid";

function AgGridScroll() {
  return (
    <>
      <div
        className="ag-theme-blue"
        style={{
          margin: "auto",
          maxWidth: "80%",
          height: 400,
        }}
      >
        <h1>Ag Grid With ScrollBar</h1>
        <Grid height="300px" width="100%"></Grid>
        <p>
          We are using Ag Grid With ScrollBar.Where you will get Horizontal and
          Vertical Scroll Bars.
        </p>
      </div>
    </>
  );
}
export default AgGridScroll;
