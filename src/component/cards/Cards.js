import React from "react";
import { Link } from "react-router-dom";

function Cards({ data, className }) {
  const { title, description, imgUrl, link } = data;
  if (!link) {
    return (
      <div className={`card ${className}`} style={{ width: "18rem" }}>
        <div className="card-header">{title}</div>
        <img className="card-img-top" src={imgUrl} alt="Card image cap" />
        <div className="card-body">
          <p className="card-text">{description}</p>
        </div>
      </div>
    );
  }
  return (
    <Link to={link}>
      <div className={`card ${className}`}>
        <div className="card-header">{title}</div>
        <img
          className="card-img-top"
          src={imgUrl}
          alt="Card image cap"
          width="200"
          height="200"
        />
        <div className="card-body">
          <p className="card-text">{description}</p>
        </div>
      </div>
    </Link>
  );
}

export default Cards;
