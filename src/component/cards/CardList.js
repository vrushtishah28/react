import React from "react";
import Cards from "./Cards";
import "./Cards.css";

function CardList({ data, className }) {
  return (
    <div className={className}>
      {data.map((item) => (
        <Cards data={item} className="mb-4"></Cards>
      ))}
    </div>
  );
}

export default CardList;
