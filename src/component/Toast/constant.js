export const toastDirectionData = [
    {
      name: "direction",
      value: "top-left",
    },
    {
      name: "direction",
      value: "top-right",
    },
    {
      name: "direction",
      value: "top-center",
    },
    {
      name: "direction",
      value: "bottom-left",
    },
    {
      name: "direction",
      value: "bottom-right",
    },
    {
      name: "direction",
      value: "bottom-center",
    },
  ];
  export const toastType = [
    {
      name: "toastType",
      type: "info",
    },
    {
      name: "toastType",
      type: "success",
    },
    {
      name: "toastType",
      type: "warning",
    },
    {
      name: "toastType",
      type: "error",
    },
    {
      name: "toastType",
      type: "default",
    },
  ];