import React from "react";
import { Link } from "react-router-dom";
function Layout({ children }) {
  return (
    <div>
      <div class="d-flex flex-column flex-md-row align-items-center  p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 class="my-0 mr-md-auto font-weight-normal linear-gradient">
          {/* <Link to="/">Component Library</Link> */}
          Component Library
        </h5>
        <img
          src="https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png"
          height="45"
          width="45"
        ></img>
      </div>
      <div>{children}</div>
    </div>
  );
}

export default Layout;
