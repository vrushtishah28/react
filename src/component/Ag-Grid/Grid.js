import React, { useState } from "react";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import "ag-grid-community/dist/styles/ag-theme-balham-dark.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";
import "ag-grid-community/dist/styles/ag-theme-alpine-dark.css";
import "ag-grid-community/dist/styles/ag-theme-blue.css";
import "ag-grid-community/dist/styles/ag-theme-bootstrap.css";
import "ag-grid-community/dist/styles/ag-theme-dark.css";
import "ag-grid-community/dist/styles/ag-theme-fresh.css";
import "ag-grid-community/dist/styles/ag-theme-material.css";
import { gridData } from "./constant";
import { getAllPost } from "./services";

function Grid({ theme, height, width, autowidth }) {
  const [data, setData] = useState({});

  const autoSizeColumns = (params) => {
    const colIds = params.columnApi
      .getAllDisplayedColumns()
      .map((col) => col.getColId());
    params.columnApi.autoSizeColumns(colIds);
  };
  function onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    params.api.sizeColumnsToFit();
    window.addEventListener("resize", function () {
      setTimeout(function () {
        params.api.sizeColumnsToFit();
      });
    });

    params.api.sizeColumnsToFit();
  }
  const setPost = async () => {
    const response = await getAllPost();
    console.log(response);
    setData({
      columnDefs: [
        {
          headerName: "userId",
          field: "userId",
          sortable: true,
          filter: true,
          resizable: true,
          suppressHorizontalScroll: true,
        },
        {
          headerName: "id",
          field: "id",
          sortable: true,
          filter: true,
          resizable: true,
          suppressHorizontalScroll: true,
        },
        {
          headerName: "title",
          field: "title",
          sortable: true,
          filter: true,
          resizable: true,
          suppressHorizontalScroll: true,
        },
        {
          headerName: "body",
          field: "body",
          sortable: true,
          filter: true,
          resizable: true,
          suppressHorizontalScroll: true,
        },
      ],
      rowData: response,
    });
  };

  setPost();
  if (data === {}) {
    return <p>Loading</p>;
  }
  return (
    <div
      className={theme}
      style={{
        maxWidth: width,
        height: height,
      }}
    >
      <AgGridReact
        pagination={true}
        paginationPageSize="10"
        domLayout="autoHeight"
        onFirstDataRendered={autowidth ? onGridReady : autoSizeColumns}
        columnDefs={data.columnDefs}
        rowData={data.rowData}
      ></AgGridReact>
    </div>
  );
}

export default Grid;
