export const gridData = {
  columnDefs: [
    { headerName: "Make", field: "make" },
    { headerName: "Model", field: "model" },
    { headerName: "Price", field: "price" },
  ],
  rowData: [
    { make: "Toyota", model: "Celica", price: 35000 },
    { make: "Ford", model: "Mondeo", price: 32000 },
    { make: "Porsche", model: "Boxter", price: 72000 },
  ],
};
export const theme = [
  "ag-theme-balham",
  "ag-theme-balham-dark",
  "ag-theme-alpine",
  "ag-theme-alpine-dark",
  "ag-theme-blue",
  "ag-theme-bootstrap",
];
export const AgGridLibrary = [
  {
    title: "AG-GRID WITH SCROLL BAR",
    description:
      "This shows Ag-Grid component with horizontal and vertical scroll bars",
    imgUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR74vzM8KbQ_P2S5-miQlP9st7qi6DeUvfnZvGngbr-9SsQU25F&amp;usqp=CAU",
    link: "/AgGrid-scroll",
  },
  {
    title: "AG-GRID AUTO WIDTH",
    description:
      "This shows Ag-Grid component with auto width and no scroll bar",
    imgUrl:
      "https://pbs.twimg.com/profile_images/961966400016404481/oEXeZFEy_400x400.jpg",
    link: "/AgGrid-noscroll",
  },
  {
    title: "AG-GRID THEMES",
    description: "This contains ag-grid themes",
    imgUrl: "https://miro.medium.com/max/802/0*KO0iuoMCzThhnCeQ.jpg",
    link: "/AgGrid-themes",
  },
];
