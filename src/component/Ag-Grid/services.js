import axios from "axios";

export const getAllPost = () => {
  return axios
    .get("https://jsonplaceholder.typicode.com/posts")
    .then((response) => response.data);
};
